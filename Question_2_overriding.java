package week3.assignment_polymorphism;
 class Shapes1{
     public void calculateArea(){
         System.out.println("i am shape");
     };
}
class Circle1 extends Shapes1 {
    public void calculateArea(){
        float r=2;
        double area=(3.14*r*r);
        System.out.println("area of Circle is "+area);
    }
}

class Triangle1 extends Circle1 {
    public void calculateArea(){
        float height=2;
        float breadth=3;
        double area=(height*breadth)/2;
        System.out.println("area of Triangle is "+area);
    }
}

public class Question_2_overriding {
    public static void main(String[] args) {
        Shapes1 shapes1;
        shapes1=new Circle1();
        shapes1.calculateArea();
        shapes1=new Triangle1();
        shapes1.calculateArea();
    }
}
/*
if I change the return type of the overriding method then it shows the error .
 so override method should be in same return type.
'calculateArea()' in 'week3.assignment_polymorphism.Circle1' clashes with 'calculateArea()'
        in 'week3.assignment_polymorphism.Shapes1'; attempting to use incompatible return type

 */