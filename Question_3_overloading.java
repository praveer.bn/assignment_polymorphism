package week3.assignment_polymorphism;
class Overloading{
    public void show(){
        System.out.println("hi");
    }
    public void show(int a){
        System.out.println("hi  "+a);
    }
    public double show(double a){
        System.out.println("hi  "+ a);
        return  a;
    }
}

public class Question_3_overloading {
    public static void main(String[] args) {
        Overloading overloading=new Overloading();
        overloading.show();
        overloading.show(1);
        overloading.show(1.1);
    }
}
/* if i change the return its not showing error bcoz overloading
is only depends on name and parameter of the methode.
 */