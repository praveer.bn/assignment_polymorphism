package week3.assignment_polymorphism;

import java.util.Scanner;

enum Signal{
    RED("STOP"),
    YELLOW("GO VERY SLOW"),
    GREEN("GO");

    public final String label;
    Signal(String label) {
        this.label = label;
    }

}

public class Question_6_enum {
    public static void main(String[] args) {
        for(Signal s : Signal.values()) {
            System.out.println(s+" MEANS "+s.label);
        }
    }
    }

