package week3.assignment_polymorphism;

abstract class Shapes{
    abstract void calculateArea();
}
class Circle extends Shapes {
    void calculateArea(){
        float r=2;
        double area=(3.14*r*r);
        System.out.println("area of Circle is "+area);
    }
}

class Triangle extends Shapes {
    void calculateArea(){
        float height=2;
        float breadth=3;
        double area=(height*breadth)/2;
        System.out.println("area of Triangle is "+area);
    }
}
class Rectangle extends Shapes {
    void calculateArea(){
        float length=2;
        float breadth=3;
        double area=length*breadth;
        System.out.println("area of Rectangle is "+area);
    }
}

public class Question_1 {
    public static void main(String[] args) {
        Shapes shapes;
        shapes=new Circle();
        shapes.calculateArea();
        shapes=new Triangle();
        shapes.calculateArea();
        shapes=new Rectangle();
        shapes.calculateArea();
    }
}
